'use strict';

angular.module('piwikButtonCreator', [
  'ngResource',
  'ui.router',
  'ngMaterial',
  'ngStorage',

  'main',
  'trackerButton',
  'customVars'
])
  .config(['$stateProvider', '$urlRouterProvider', '$mdThemingProvider',
    function ($stateProvider, $urlRouterProvider, $mdThemingProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'main.MainCtrl'
      });

    $mdThemingProvider.theme('default');

    $urlRouterProvider.otherwise('/');
  }])
  .directive('piwikScript', function ($window, $mdToast) {
    var directive = {

      restrict: 'E',
      scope: {
        trackerUrl: '=',
        siteId: '='
      },
      link: function(scope) {
        var _paq = $window._paq = _paq || [];
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function() {
          var u = scope.trackerUrl;
          _paq.push(['setTrackerUrl', u+'piwik.php']);
          _paq.push(['setSiteId', scope.siteId]);
          var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
          g.type='text/javascript';
          g.onload = function () {
            $mdToast.show($mdToast.simple()
              .content('Piwik JS is loaded!')
                .position('bottom right')
                .hideDelay(3000)
            );
          };
          g.async=true; g.defer=true; g.src=u+'piwik.js';
          s.parentNode.insertBefore(g,s);
        })();
      }
    };

    return directive;
  })
;
