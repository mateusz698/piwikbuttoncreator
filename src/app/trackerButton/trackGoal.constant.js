(function (angular) {
  'use strict';

  var MODULE_NAME = 'trackerButton';

  angular
    .module(MODULE_NAME)
    .constant(MODULE_NAME +'.Goal', {
      'categoryName': 'Track Goal',
      'requestFunction': 'trackGoal',
      'buttons': [
        [
          {
            'name': 'Goal #1',
            'args': ['1', '324'] // [idGoal, customRevenue]
          },
          {
            'name': 'Goal #2',
            'args': ['2', '454'] // [idGoal, customRevenue]
          }
        ]
      ]
    }
  );

}(angular));
