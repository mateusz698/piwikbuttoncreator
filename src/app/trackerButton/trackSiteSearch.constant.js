(function (angular) {
  'use strict';

  var MODULE_NAME = 'trackerButton';

  angular
    .module(MODULE_NAME)
    .constant(MODULE_NAME +'.SiteSearch', {
      'categoryName': 'Track Site Search',
      'requestFunction': 'trackSiteSearch',
      'buttons': [
        [
          {
            'name': 'Search XYZ for Global',
            'args': ['XYZ', 'global', '101'] // [keyword, category, resultCount]
          },
          {
            'name': 'Search ABC for Global',
            'args': ['ABC', 'global', '102'] // [keyword, category, resultCount]
          }
        ]
      ]
    }
  );

}(angular));
