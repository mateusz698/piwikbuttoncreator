(function (angular) {
  'use strict';

  var MODULE_NAME = 'trackerButton';

  angular
    .module(MODULE_NAME)
    .constant(MODULE_NAME +'.Link', {
      'categoryName': 'Track Link',
      'requestFunction': 'trackLink',
      'buttons': [
        [
          {
            'name': 'Link #1',
            'args': ['http://google.com', 'Download'] // [url, linkType]
          },
          {
            'name': 'Link #2',
            'args': ['http://clearcode.cc', 'link'] // [url, linkType]
          }
        ]
      ]
    }
  );

}(angular));
