(function (angular) {
  'use strict';

  var MODULE_NAME = 'trackerButton';

  angular
    .module(MODULE_NAME)
    .constant(MODULE_NAME +'.Events', {
      'categoryName': 'Track Events',
      'requestFunction': 'trackEvent',
      'buttons': [
        [
          {
            'name': 'Event Category: Download',
            'args': ['eventCategory', 'download'] // [category, action, name, value]
          },
          {
            'name': 'Event Category: Share',
            'args': ['eventCategory', 'share'] // [category, action, name, value]
          },
          {
            'name': 'Event Category: Like',
            'args': ['eventCategory', 'like'] // [category, action, name, value]
          }
        ], [
          {
            'name': 'Launch: Native',
            'args': ['launch', 'native'] // [category, action, name, value]
          },
          {
            'name': 'Launch: WebApp',
            'args': ['launch', 'webApp'] // [category, action, name, value]
          }
        ], [
          {
            'name': 'Use Features: Settings',
            'args': ['useFeature', 'settings'] // [category, action, name, value]
          },
          {
            'name': 'Use Features: Player',
            'args': ['useFeature', 'player'] // [category, action, name, value]
          }
        ], [
          {
            'name': 'Show Content: Video',
            'args': ['showContent', 'video'] // [category, action, name, value]
          },
          {
            'name': 'Show Content: Text',
            'args': ['showContent', 'text'] // [category, action, name, value]
          }
        ]
      ]
    }
  );

}(angular));
