(function (angular) {
  'use strict';

  var MODULE_NAME = 'trackerButton';

  angular
    .module(MODULE_NAME)
    .constant(MODULE_NAME +'.PageView', {
      'categoryName': 'Track Page View',
      'requestFunction': 'trackPageView',
      'buttons': [
        [
          {
            'name': 'QC_Lingo_App_Title',
            'args': ['QC_Lingo_App_Title'] // [customTitle]
          },
          {
            'name': 'Another_App_Title',
            'args': ['Another_App_Title'] // [customTitle]
          }
        ]
      ]
    }
  );

}(angular));
