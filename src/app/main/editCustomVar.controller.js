(function (angular) {
  'use strict';

  var MODULE_NAME, injectParams;
  MODULE_NAME = 'main';
  injectParams = [
    '$scope',
    '$mdDialog',
    'options'
  ];

  function EditCustomVarCtrl($scope, $mdDialog, options) {
    $scope.options = options;
    $scope.customVar = options.cv;
    $scope.scopes = [
      'visit', 'page'
    ];

    $scope.close = function() {
      $mdDialog.cancel();
    };
    $scope.save = function(customVar) {
      $mdDialog.hide(customVar);
    };
  }

  EditCustomVarCtrl.$inject = injectParams;


  angular
    .module(MODULE_NAME)
    .controller(MODULE_NAME + '.EditCustomVarCtrl', EditCustomVarCtrl);

}(angular));
