(function (angular) {
  'use strict';

  var MODULE_NAME, injectParams;

  MODULE_NAME = 'main';
  injectParams = [
    'trackerButton.Events',
    'trackerButton.PageView',
    'trackerButton.SiteSearch',
    'trackerButton.Goal',
    'trackerButton.Link'
  ];

  function MainFactory(TrackEvents, TrackPageView, TrackSiteSearch, TrackGoal, TrackLink) {

    return {
      actions: [
        TrackEvents, TrackPageView, TrackSiteSearch, TrackGoal, TrackLink
      ]
    }
  }

  MainFactory.$inject = injectParams;

  angular
    .module(MODULE_NAME)
    .factory(MODULE_NAME + '.MainFactory', MainFactory);

}(angular));
