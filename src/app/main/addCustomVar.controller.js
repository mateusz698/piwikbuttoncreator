(function (angular) {
  'use strict';

  var MODULE_NAME, injectParams;
  MODULE_NAME = 'main';
  injectParams = [
    '$scope',
    '$mdDialog',
    'options'
  ];

  function AddCustomVarCtrl($scope, $mdDialog, options) {
    $scope.options = options;
    $scope.customVar = {
      index: '',
      name: '',
      value: '',
      scope: '',
      selected: false
    };
    $scope.scopes = [
      'visit', 'page'
    ];

    $scope.close = function() {
      $mdDialog.cancel();
    };
    $scope.save = function(customVar) {
      $mdDialog.hide(customVar);
    };
  }

  AddCustomVarCtrl.$inject = injectParams;


  angular
    .module(MODULE_NAME)
    .controller(MODULE_NAME + '.AddCustomVarCtrl', AddCustomVarCtrl);

}(angular));
