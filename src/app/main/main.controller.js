(function (angular) {
  'use strict';

  var MODULE_NAME, injectParams;
  MODULE_NAME = 'main';
  injectParams = [
    '$scope',
    '$window',
    '$log',
    '$mdDialog',
    '$localStorage',
    MODULE_NAME + '.MainFactory',
    'customVars.CVService'
  ];

  function MainCtrl($scope, $window, $log, $mdDialog, $localStorage, MainFactory, CVService) {
    var $storage;
    $scope.tracker = MainFactory;
    $scope.cvServ = CVService;
    $storage = $localStorage.$default({
      siteId: '',
      userId: ''
    });

    $scope.request = function (btn, trackerFunction) {
      var piwikTracker, cvPages, cvVisits;

      cvPages = CVService.getAllSelected('page');
      cvVisits = CVService.getAllSelected('visit');

      if (!!$window.Piwik) {
        piwikTracker = $window.Piwik.getTracker();
        angular.forEach(cvPages, function (elem) {
          piwikTracker.setCustomVariable(elem.index, elem.name, elem.value, 'page');
        });
        angular.forEach(cvVisits, function (elem) {
          piwikTracker.setCustomVariable(elem.index, elem.name, elem.value, 'visit');
        });

        if (!!$storage.siteId && $storage.siteId.length > 0) {
          console.log($storage.siteId);
          piwikTracker.setSiteId( parseInt($storage.siteId) );
        }
        if (!!$storage.userId && $storage.userId.length > 0) {
          piwikTracker.setUserId( $storage.userId );
        }

        //piwikTracker.setUserId('gerge');
        piwikTracker[trackerFunction].apply(piwikTracker, btn.args);
      } else {
        $log.log('Error. Piwik module doesn\'t loaded.');
      }
    };

    $scope.addCustomVar = function(ev) {
      $mdDialog.show({
        controller: MODULE_NAME + '.AddCustomVarCtrl',
        templateUrl: 'app/main/addCustomVar.tmpl.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        locals: {
          options: {
            head: 'Add Custom Var',
            mode: 'add'
          }
        }
      })
        .then(function(customVar) {
          CVService.add(customVar);
        });
    };

    $scope.editCustomVar = function (ev, cv) {
      ev.stopPropagation();
      $mdDialog.show({
        controller: MODULE_NAME + '.EditCustomVarCtrl',
        templateUrl: 'app/main/addCustomVar.tmpl.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        locals: {
          options: {
            head: 'Edit Custom Var',
            cv: cv,
            mode: 'edit'
          }
        }
      });
    };

    $scope.storage = CVService.getAll();
  }

  MainCtrl.$inject = injectParams;

  angular
    .module(MODULE_NAME)
    .controller(MODULE_NAME + '.MainCtrl', MainCtrl);

}(angular));
