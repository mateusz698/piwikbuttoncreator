(function (angular) {
  'use strict';

  var injectParams, MODULE_NAME;
  MODULE_NAME = 'customVars';
  injectParams = ['$localStorage', '$filter'];

  function CustomVarsService($localStorage, $filter) {
    var service, $storage;

    service = this;
    $storage = $localStorage.$default({
      page: [],
      visit: []
    });

    service.add = function add(customVar) {
      if (customVar.scope === 'page') {
        $storage.page.push(customVar);
      } else if (customVar.scope === 'visit') {
        $storage.visit.push(customVar);
      }
    };

    service.getAll = function getAll() {
      return $storage;
    };

    service.getAllSelected = function getAllSelected(scope) {
      return $filter('filter')($storage[scope], {selected: true});
    };

    service.remove = function remove(cv, scope) {
      $storage[scope] = $filter('filter')($storage[scope], function (elem) {
        return !angular.equals(elem, cv);
      });
    };
  }

  CustomVarsService.$inject = injectParams;

  angular.module(MODULE_NAME)
    .service(MODULE_NAME + '.CVService', CustomVarsService);

}(angular));
